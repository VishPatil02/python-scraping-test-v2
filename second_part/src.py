# Your code here

# task 4

class CacheDecorator:
    """Saves the results of a function according to its parameters"""

    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap


# task 5

class LoginMetaClass(type):
    def __new__(cls, name, bases, dct):
        for key, value in dct.items():
            if callable(value) and key != '__init__':
                dct[key] = cls.wrap_method(value)
        return super().__new__(cls, name, bases, dct)

    @staticmethod
    def wrap_method(method):
        def wrapped(self, *args, **kwargs):
            if not getattr(self, 'logged_in', False):
                raise PermissionError("Access denied: You must be logged in.")
            return method(self, *args, **kwargs)

        return wrapped


class AccessWebsite(metaclass=LoginMetaClass):
    logged_in = False

    def login(self, username, password):
        if username == "admin" and password == "admin":
            self.logged_in = True

    def access_website(self):
        return "Success"
