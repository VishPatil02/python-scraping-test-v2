# Second Part - Task 1

import json
import time


def http_request(url):
    headers = {
        'Accept': 'application/json'
    }

    max_retries = 3
    retries = 0

    while retries < max_retries:
        try:
            response = requests.get(url, headers=headers)
            if response.status_code == 200:
                try:
                    json_response = response.json()
                    print(json_response)
                    break
                except json.JSONDecodeError:
                    print("Response is not in valid JSON format.")
            else:
                print("Unexpected status code:", response.status_code)
        except requests.exceptions.RequestException as e:
            print("Error:", e)

        # Increment retries and wait before retrying
        retries += 1
        if retries < max_retries:
            print("Retrying...")
            time.sleep(1)  # Wait for 1 second before retrying


# URL to send the GET request
url = 'https://eoa0qzkprh8wd6s.m.pipedream.net'

# Call the function with the URL
http_request(url)

# -----------------------------------------------------------------------------------------------------------------

# Second Part - Task 2

import requests

headers = {
    'Accept': 'application/json',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36',
}

url = 'https://eo6f3hp6twlkn24.m.pipedream.net/'

for i in range(10):
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        break

# -----------------------------------------------------------------------------------------------------------
# Second Part - Task 3

from datetime import datetime, timedelta


def format_date(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        formatted_date = result.strftime('%Y-%m/%d')
        return formatted_date

    return wrapper


@format_date
def first_day_of_last_week():
    today = datetime.today()
    year = today.year
    month = today.month
    if month == 12:
        next_month = 1
        next_year = year + 1
    else:
        next_month = month + 1
        next_year = year

    first_day_next_month = datetime(next_year, next_month, 1)
    last_day_current_month = first_day_next_month - timedelta(days=1)
    # Calculate the number of days to subtract to get to the first day of the last week
    days_to_subtract = (last_day_current_month.weekday() + 1) % 7
    first_day_last_week = last_day_current_month - timedelta(days=days_to_subtract)
    return first_day_last_week


# Example usage
print(first_day_of_last_week())
