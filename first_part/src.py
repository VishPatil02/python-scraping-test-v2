# First Part - Task 1
def find_most_recurrent_sequence(keywords):
    # Initialize a dictionary to store sequences and their frequencies
    sequence_freq = {}

    # Iterate through each word in the list of keywords
    for word in keywords:
        # Iterate through each character in the word, excluding the last character
        for i in range(len(word) - 1):
            # Extract a sequence of two characters
            sequence = word[i:i + 2]
            # Increment the frequency count of the sequence in the dictionary
            sequence_freq[sequence] = sequence_freq.get(sequence, 0) + 1

    # Find the sequence with the highest frequency
    most_recurrent_sequence = max(sequence_freq, key=sequence_freq.get)

    return most_recurrent_sequence


# Given list of keywords
list1 = ['milk', 'chocolate', 'c+', 'python', 'cat', 'dog']

# Find the most recurrent sequence
result = find_most_recurrent_sequence(list1)
print("The most recurrent sequence is:", result)
# --------------------------------------------------------------------------------------------------

# First Part - Task 2

import re


def extract_nutritional_info(text):
    # Initialize an empty dictionary to store the extracted information
    nutrition_dict = {}

    # Regular expression pattern to match nutritional information
    pattern = r'([A-Za-zÀ-ÖØ-öø-ÿ\s\(\)\-0-9]+?)\s*:\s*([0-9.,\s]+(?:UI|mg|g))'

    # Find all matches in the text
    matches = re.findall(pattern, text)

    # Iterate over the matches and build the dictionary
    for match in matches:
        # Handling special cases where the nutrition name contains a special character
        nutrition_name = match[0].strip().replace('–', '').replace("'", "")
        nutrition_dict[nutrition_name] = match[1].strip()

    return nutrition_dict


# Given text containing nutritional information
text = 'Additifs nutritionnels : Vitamine C-D3 : 160 UI, Fer (3b103) : 4mg, Iode (3b202) : 0,28 mg, Cuivre (3b405, 3b406) : 2,2 mg,Manganèse (3b502, 3b503, 3b504) : 1,1 mg, Zinc (3b603,3b605, 3b606) : 11 mg –Clinoptilolited’origine sédimentaire : 2 g. Protéine : 11,0 % - Teneur en matières grasses : 4,5 % - Cendres brutes : 1,7 % - Cellulose brute : 0,5 % - Humidité : 80,0 %.'

# Extract nutritional information and build the dictionary
nutrition_info = extract_nutritional_info(text)
print(nutrition_info)


# ------------------------------------------------------------------------------
# First Part - Task 3

def is_grandma_list(lst):
    def has_adjacent_product(sub_lst):
        for i in range(len(sub_lst) - 1):
            if isinstance(sub_lst[i], list):
                if has_adjacent_product(sub_lst[i]):
                    return True
            if isinstance(sub_lst[i + 1], list):
                if has_adjacent_product(sub_lst[i + 1]):
                    return True
            if isinstance(sub_lst[i], int) and isinstance(sub_lst[i + 1], int):
                return True
        return False

    for item in lst:
        if isinstance(item, list):
            if has_adjacent_product(item):
                return "Its a grandma list"  # True
    return "Its not a grandma list"  # False


# Test cases
list1 = [1, 2, [[4, 5], [4, 7]], 5, 4, [[95], [2]]]
list2 = [5, 9, 4, [[8, 7]], 4, 7, 1, [[5]]]

print(is_grandma_list(list1))
print(is_grandma_list(list2))
